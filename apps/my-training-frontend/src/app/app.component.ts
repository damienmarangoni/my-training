import { Component } from '@angular/core';

@Component({
  selector: 'my-training-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'my-training-frontend';
}
